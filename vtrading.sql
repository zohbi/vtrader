-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Apr 01, 2021 at 03:41 AM
-- Server version: 10.3.28-MariaDB-log-cll-lve
-- PHP Version: 7.3.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `planbvaf_vtraderApp`
--

-- --------------------------------------------------------

--
-- Table structure for table `boards`
--

CREATE TABLE `boards` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `boardTypeId` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `board_templates`
--

CREATE TABLE `board_templates` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `board_templates`
--

INSERT INTO `board_templates` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'basic', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `board_template_widgets`
--

CREATE TABLE `board_template_widgets` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `boardTemplateId` int(11) NOT NULL,
  `widgetId` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tagId` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `left` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `top` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `width` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `height` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `board_template_widgets`
--

INSERT INTO `board_template_widgets` (`id`, `boardTemplateId`, `widgetId`, `tagId`, `left`, `top`, `width`, `height`, `created_at`, `updated_at`) VALUES
(1, 1, '1', 'tagId-1', '24', '0', '614', '298', '2021-03-17 00:31:06', '2021-03-25 00:43:40'),
(2, 1, '1', 'tagId-2', '712', '345', '240', '420', '2021-03-17 00:31:06', '2021-03-25 00:43:43'),
(3, 1, '1', 'tagId-3', '336', '338', '360', '420', '2021-03-17 00:31:06', '2021-03-25 00:43:45'),
(4, 1, '1', 'tagId-4', '648', '0', '314', '313', '2021-03-17 21:40:50', '2021-03-25 00:43:38'),
(5, 1, '1', 'tagId-5', '0', '340', '320', '410', '2021-03-17 21:41:07', '2021-03-25 00:43:46');

-- --------------------------------------------------------

--
-- Table structure for table `board_types`
--

CREATE TABLE `board_types` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `board_types`
--

INSERT INTO `board_types` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Blank', NULL, NULL),
(2, 'Normal', NULL, NULL),
(3, 'Multi Trades', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `board_widget_positions`
--

CREATE TABLE `board_widget_positions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `userId` int(11) NOT NULL,
  `boardId` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `widgetId` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tagId` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `left` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `top` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `width` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `height` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `board_widget_positions`
--

INSERT INTO `board_widget_positions` (`id`, `userId`, `boardId`, `widgetId`, `tagId`, `left`, `top`, `width`, `height`, `created_at`, `updated_at`) VALUES
(1, 2, '2', '1', 'tagId-1', '0', '0', '634', '298', '2021-03-17 05:31:06', '2021-03-30 10:11:39'),
(2, 2, '2', '1', 'tagId-2', '1032', '0', '240', '420', '2021-03-17 05:31:06', '2021-03-30 10:12:47'),
(3, 2, '2', '1', 'tagId-3', '1308', '0', '375', '420', '2021-03-17 05:31:06', '2021-03-30 10:12:45'),
(4, 2, '2', '1', 'tagId-4', '590', '0', '376', '279', '2021-03-18 02:40:50', '2021-03-30 10:12:41'),
(5, 2, '2', '1', 'tagId-5', '0', '344', '633', '410', '2021-03-18 02:41:07', '2021-03-30 10:11:53'),
(43, 3, '2', '1', 'tagId-43', '24', '0', '614', '298', '2021-03-25 07:00:29', '2021-03-25 07:00:29'),
(44, 3, '2', '1', 'tagId-44', '712', '345', '240', '420', '2021-03-25 07:00:29', '2021-03-25 07:00:29'),
(45, 3, '2', '1', 'tagId-45', '336', '338', '360', '420', '2021-03-25 07:00:29', '2021-03-25 07:00:29'),
(46, 3, '2', '1', 'tagId-46', '648', '0', '314', '313', '2021-03-25 07:00:29', '2021-03-25 07:00:29'),
(47, 3, '2', '1', 'tagId-47', '0', '340', '320', '410', '2021-03-25 07:00:29', '2021-03-25 07:00:29'),
(53, 5, '2', '1', 'tagId-53', '24', '0', '614', '298', '2021-03-26 14:46:50', '2021-03-26 14:46:50'),
(54, 5, '2', '1', 'tagId-54', '712', '345', '240', '420', '2021-03-26 14:46:50', '2021-03-26 14:46:50'),
(57, 5, '2', '1', 'tagId-57', '116', '311', '454', '410', '2021-03-26 14:46:50', '2021-03-31 18:46:54'),
(58, 6, '2', '1', 'tagId-58', '24', '0', '614', '298', '2021-03-31 17:57:47', '2021-03-31 17:57:47'),
(59, 6, '2', '1', 'tagId-59', '971', '4', '240', '306', '2021-03-31 17:57:47', '2021-03-31 17:59:12'),
(60, 6, '2', '1', 'tagId-60', '13', '319', '434', '414', '2021-03-31 17:57:47', '2021-03-31 17:59:48'),
(61, 6, '2', '1', 'tagId-61', '648', '0', '314', '313', '2021-03-31 17:57:47', '2021-03-31 17:57:47'),
(62, 6, '2', '1', 'tagId-62', '463', '322', '358', '406', '2021-03-31 17:57:47', '2021-03-31 17:59:56'),
(64, 5, '2', '1', 'tagId-64', '1139', '164', '385', '420', '2021-03-31 18:46:56', '2021-03-31 18:47:00'),
(66, 5, '2', '1', 'tagId-66', '528', '13', '240', '420', '2021-03-31 18:47:05', '2021-03-31 18:47:14');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(4, '2014_10_12_000000_create_users_table', 1),
(5, '2014_10_12_100000_create_password_resets_table', 1),
(6, '2014_10_12_200000_add_two_factor_columns_to_users_table', 1),
(7, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
(8, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
(9, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
(10, '2016_06_01_000004_create_oauth_clients_table', 1),
(11, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
(12, '2019_08_19_000000_create_failed_jobs_table', 1),
(13, '2021_03_17_083530_add_user_type_to_users_table', 2),
(14, '2021_03_17_083834_create_user_types_table', 2),
(15, '2021_03_17_095807_create_board_element_positions_table', 3),
(16, '2021_03_17_100131_create_board_widgets_table', 3),
(17, '2021_03_17_100147_create_boards_table', 3),
(18, '2021_03_17_100306_create_board_types_table', 3),
(19, '2021_03_25_114157_create_board_templates_table', 4),
(20, '2021_03_25_114442_create_board_template_widgets_table', 5),
(21, '2021_03_25_104031_create_verify_email_code', 6),
(22, '2021_03_30_103949_add_columns_in_users', 7);

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `client_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `client_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `provider`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
('92f8b62b-2ab2-4984-bbe4-e74c1a18499c', NULL, 'Laravel Personal Access Client', 'e9y7SX4lPyR9tVD6m85gmy7F5ibuPLt77mmlWpSC', NULL, 'http://localhost', 1, 0, 0, '2021-03-17 03:17:38', '2021-03-17 03:17:38'),
('92f8b62b-3148-45e9-86bf-94b4e0f048ce', NULL, 'Laravel Password Grant Client', 'sfbzSLP5up8PPaeAHDIeplMkDeSfj6bfsTtavldA', 'users', 'http://localhost', 0, 1, 0, '2021-03-17 03:17:38', '2021-03-17 03:17:38');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, '92f8b62b-2ab2-4984-bbe4-e74c1a18499c', '2021-03-17 03:17:38', '2021-03-17 03:17:38');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `facebook_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `google_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `two_factor_secret` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `two_factor_recovery_codes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `themeSetting` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `userTypeId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `facebook_id`, `google_id`, `two_factor_secret`, `two_factor_recovery_codes`, `remember_token`, `themeSetting`, `created_at`, `updated_at`, `userTypeId`) VALUES
(1, 'admin', 'admin@vtraders.com', NULL, '$2y$10$kPOQgjmAkVaXv9g4O3vUcOjRYb5/xjY5/p3G7.BqYaidoftRM/hD.', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-17 03:21:05', '2021-03-17 03:21:05', 1),
(2, 'trader 1', 'trader@gmail.com', NULL, '$2y$10$tldfJ.mzxY5tsw7wIGzrwu/P1RMGZcqk7zG3XoUHvhVwbbbSRHMOu', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-17 04:20:31', '2021-03-17 04:20:31', 2),
(3, 'trader 2', 'trader2@gmail.com', NULL, '$2y$10$ulg0pcp.O1AweS340xfEKO2JA2hcNX4BO20RP.fqqtcuDfHdQ4/QO', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-22 07:22:15', '2021-03-22 07:22:15', 2),
(5, 'waqar', 'waqaryounus277@gmail.com', NULL, '$2y$10$9Q39kx0VzG983w2iFy.XtOzvGMLXj00f/o1VEScaAukTbQKvEZqDi', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-26 14:46:32', '2021-03-26 16:18:19', 2),
(6, 'Roxas Zohbi', 'roxas.zohbi786@gmail.com', NULL, '$2y$10$Oxfopg530roI0KJcW529e.pa8YsaqTnt65c4qoDhXvubSclHqDUZm', NULL, '114895162608283517548', NULL, NULL, NULL, NULL, '2021-03-31 16:55:01', '2021-03-31 16:55:01', 2);

-- --------------------------------------------------------

--
-- Table structure for table `user_types`
--

CREATE TABLE `user_types` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_types`
--

INSERT INTO `user_types` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'admin', NULL, NULL),
(2, 'trader', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `verify_email_code`
--

CREATE TABLE `verify_email_code` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `verify_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `verify_email_code`
--

INSERT INTO `verify_email_code` (`id`, `verify_code`, `email`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, '409339', 'waqaryounus277@gmail.com', '2021-03-26 14:46:32', '2021-03-26 14:43:13', '2021-03-26 14:46:32');

-- --------------------------------------------------------

--
-- Table structure for table `widgets`
--

CREATE TABLE `widgets` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `widgets`
--

INSERT INTO `widgets` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Candle -Stick Chart', NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `boards`
--
ALTER TABLE `boards`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `board_templates`
--
ALTER TABLE `board_templates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `board_template_widgets`
--
ALTER TABLE `board_template_widgets`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `board_types`
--
ALTER TABLE `board_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `board_widget_positions`
--
ALTER TABLE `board_widget_positions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_auth_codes_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `user_types`
--
ALTER TABLE `user_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `verify_email_code`
--
ALTER TABLE `verify_email_code`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `widgets`
--
ALTER TABLE `widgets`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `boards`
--
ALTER TABLE `boards`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `board_templates`
--
ALTER TABLE `board_templates`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `board_template_widgets`
--
ALTER TABLE `board_template_widgets`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `board_types`
--
ALTER TABLE `board_types`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `board_widget_positions`
--
ALTER TABLE `board_widget_positions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `user_types`
--
ALTER TABLE `user_types`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `verify_email_code`
--
ALTER TABLE `verify_email_code`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `widgets`
--
ALTER TABLE `widgets`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
