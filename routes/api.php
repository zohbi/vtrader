<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ApisController;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Auth\RegisteredUserController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('/sendCode', [RegisteredUserController::class, "AjaxCallForEmailVerificationCode"] );

Route::middleware("auth:api")->group(function ()
{ 
    Route::get('/user', function (Request $request) {
        return $request->user();
    });
    Route::post('/changePostion',   [ApisController::class, "changePostion"] );
    Route::post('/deleteWidget',    [ApisController::class, "deleteWidget"] );
    Route::post('/addWidget',       [ApisController::class, "addWidget"] );
    Route::post('/addBoard',        [ApisController::class, "addBoard"] );
    Route::post('/removeBoard',     [ApisController::class, "removeBoard"] );
    Route::post('/editBoardName',   [ApisController::class, "editBoardName"] );
    Route::post('/userTheme',        [ApisController::class,"userTheme"]);
});


