<?php

use App\Http\Controllers\AppController;
use App\Http\Controllers\Auth\ForgetPasswordResetController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;
use App\Http\Controllers\GoogleController;
use App\Http\Controllers\FacebookController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

require __DIR__.'/auth.php';
Route::get('/', function () {
    if(Auth::check()){
        return Redirect::to('/board');
    }else{
        return Redirect::to('/login');
    }
});

Route::get('/home', function () {
    return Inertia::render('Home');
})->middleware(['auth'])->name("home");

Route::get('/reset-email', function(){
    return view('auth.passwords.sendemail');
})->name("forget.email");

Route::get('/checking', function () {
    return Inertia::render('Checking');
})->name("checking");

Route::get('/checkAuth', function (){
    return json_encode(array(
        "auth"          => Auth::check(),
        "themeSetting"  => Auth::check()?Auth::user()->themeSetting:""
    ));
});

Route::group(['middleware' => 'auth'], function(){
    //  GET REQUEST
    Route::get('/profile',          [AppController::class, "profile"])          ->name("profile");
    Route::get('/board',            [AppController::class, "board"])            ->name("board");
    Route::get('/customizeBoard',   [AppController::class, "customizeBoard"])   ->name("customizeBoard");
    Route::get('/watchlist',        [AppController::class, "Watchlist"])        ->name("Watchlist");
    Route::get('/orderform',        [AppController::class, "Orderform"])        ->name("orderform");
    Route::get('/multitab',         [AppController::class, "MultiTab"])         ->name("multitab");
    Route::get('/account',          [AppController::class, "account"])          ->name("account");
    // POST REQUEST
    Route::post('/customizeBoard',  [AppController::class, "customizeBoard"])   ->name("customizeBoard");
});

Route::get('/resetPassword/{email_encode}',     [ForgetPasswordResetController::class, "ResetPassword"])                    ->name('resetPassword'); // Forgot Password send email 
Route::post('/SetNewPassword',                  [ForgetPasswordResetController::class, "SetNewPassword"])                   ->name('SetNewPassword'); // Set New Password 
Route::post('/AjaxCallForForgotPasswordEmail',  [ForgetPasswordResetController::class, "AjaxCallForForgotPasswordEmail"] )  ->name('AjaxCallForForgotPasswordEmail'); // Forgot Password send email 

Route::get('auth/google', [GoogleController::class, 'redirectToGoogle']);
Route::get('auth/google/callback', [GoogleController::class, 'handleGoogleCallback']);

Route::get('auth/facebook', [FacebookController::class, 'redirectToFacebook']);
Route::get('auth/facebook/callback', [FacebookController::class, 'handleFacebookCallback']);
