<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Widgets;
use App\Models\BoardTypes;
use App\Models\BoardTemplateWidgets;
class BoardTypeWidgetSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        BoardTypes::firstOrCreate(["user_id" => '1',"name" => "Blank", "image"=> "/assets/img/add-board.png"]);
        BoardTypes::firstOrCreate(["user_id" => '1',"name" => "Normal", "image"=> "/assets/img/add-board2.png"]);
        BoardTypes::firstOrCreate(["user_id" => '1',"name" => "2 Trades Vertical", "image"=> "/assets/img/add-board3.png"]);
        BoardTypes::firstOrCreate(["user_id" => '1',"name" => "2 Trades Horizontal"]);
        BoardTypes::firstOrCreate(["user_id" => '1',"name" => "3 Trades Vertical"]);
        BoardTypes::firstOrCreate(["user_id" => '1',"name" => "3 Trades Horizontal"]);
        BoardTypes::firstOrCreate(["user_id" => '1',"name" => "4 Trades Vertical"]);
        BoardTypes::firstOrCreate(["user_id" => '1',"name" => "4 Trades Horizontal"]);

        BoardTemplateWidgets::firstOrCreate([
            'boardTypeId' => 2,
            'widgetId' => 1,
            'tagId' => 'tagId-1',
            'left' => 24,
            'top' => 0,
            'width' => 614,
            'height' =>298
        ]);
        BoardTemplateWidgets::firstOrCreate([
            'boardTypeId' => 2,
            'widgetId' => 1,
            'tagId' => 'tagId-2',
            'left' => 712,
            'top' => 345,
            'width' => 240,
            'height' =>420
        ]);
        BoardTemplateWidgets::firstOrCreate([
            'boardTypeId' => 2,
            'widgetId' => 1,
            'tagId' => 'tagId-3',
            'left' => 336,
            'top' => 338,
            'width' => 360,
            'height' =>420
        ]);
        BoardTemplateWidgets::firstOrCreate([
            'boardTypeId' => 2,
            'widgetId' => 1,
            'tagId' => 'tagId-4',
            'left' => 648,
            'top' => 0,
            'width' => 314,
            'height' =>313
        ]);
        BoardTemplateWidgets::firstOrCreate([
            'boardTypeId' => 2,
            'widgetId' => 1,
            'tagId' => 'tagId-5',
            'left' => 0,
            'top' => 340,
            'width' => 320,
            'height' =>410
        ]);
        BoardTemplateWidgets::firstOrCreate([
            'boardTypeId'   => 3,
            'widgetId'      => 1,
            'tagId'         => 'tagId-6',
            'left'          => 0,
            'top'           => 0,
            'width'         => 800,
            'height'        => 880
        ]);
        BoardTemplateWidgets::firstOrCreate([
            'boardTypeId'   => 3,
            'widgetId'      => 1,
            'tagId'         => 'tagId-7',
            'left'          => 838,
            'top'           => 0,
            'width'         => 800,
            'height'        => 880
        ]);
        BoardTemplateWidgets::firstOrCreate([
            'boardTypeId'   => 4,
            'widgetId'      => 1,
            'tagId'         => 'tagId-8',
            'left'          => 0,
            'top'           => 0,
            'width'         => 1810,
            'height'        => 445
        ]);
        BoardTemplateWidgets::firstOrCreate([
            'boardTypeId'   => 4,
            'widgetId'      => 1,
            'tagId'         => 'tagId-9',
            'left'          => 0,
            'top'           => 471,
            'width'         => 1810,
            'height'        => 445
        ]);
        BoardTemplateWidgets::firstOrCreate([
            'boardTypeId'   => 5,
            'widgetId'      => 1,
            'tagId'         => 'tagId-10',
            'left'          => 0,
            'top'           => 0,
            'width'         => 550,
            'height'        => 880
        ]);
        BoardTemplateWidgets::firstOrCreate([
            'boardTypeId'   => 5,
            'widgetId'      => 1,
            'tagId'         => 'tagId-11',
            'left'          => 576,
            'top'           => 0,
            'width'         => 550,
            'height'        => 880
        ]);
        BoardTemplateWidgets::firstOrCreate([
            'boardTypeId'   => 5,
            'widgetId'      => 1,
            'tagId'         => 'tagId-12',
            'left'          => 1146,
            'top'           => 0,
            'width'         => 550,
            'height'        => 880
        ]);
        BoardTemplateWidgets::firstOrCreate([
            'boardTypeId'   => 6,
            'widgetId'      => 1,
            'tagId'         => 'tagId-13',
            'left'          => 0,
            'top'           => 0,
            'width'         => 1700,
            'height'        => 360
        ]);
        BoardTemplateWidgets::firstOrCreate([
            'boardTypeId'   => 6,
            'widgetId'      => 1,
            'tagId'         => 'tagId-14',
            'left'          => 0,
            'top'           => 388,
            'width'         => 1700,
            'height'        => 360
        ]);
        BoardTemplateWidgets::firstOrCreate([
            'boardTypeId'   => 6,
            'widgetId'      => 1,
            'tagId'         => 'tagId-15',
            'left'          => 0,
            'top'           => 788,
            'width'         => 1700,
            'height'        => 360
        ]);
        BoardTemplateWidgets::firstOrCreate([
            'boardTypeId'   => 7,
            'widgetId'      => 1,
            'tagId'         => 'tagId-16',
            'left'          => 0,
            'top'           => 0,
            'width'         => 445,
            'height'        => 975
        ]);
        BoardTemplateWidgets::firstOrCreate([
            'boardTypeId'   => 7,
            'widgetId'      => 1,
            'tagId'         => 'tagId-17',
            'left'          => 470,
            'top'           => 0,
            'width'         => 445,
            'height'        => 975
        ]);
        BoardTemplateWidgets::firstOrCreate([
            'boardTypeId'   => 7,
            'widgetId'      => 1,
            'tagId'         => 'tagId-18',
            'left'          => 940,
            'top'           => 0,
            'width'         => 445,
            'height'        => 975
        ]);
        BoardTemplateWidgets::firstOrCreate([
            'boardTypeId'   => 7,
            'widgetId'      => 1,
            'tagId'         => 'tagId-19',
            'left'          => 1411,
            'top'           => 0,
            'width'         => 445,
            'height'        => 975
        ]);
        BoardTemplateWidgets::firstOrCreate([
            'boardTypeId'   => 8,
            'widgetId'      => 1,
            'tagId'         => 'tagId-19',
            'left'          => 0,
            'top'           => 0,
            'width'         => 2030,
            'height'        => 445
        ]);
        BoardTemplateWidgets::firstOrCreate([
            'boardTypeId'   => 8,
            'widgetId'      => 1,
            'tagId'         => 'tagId-20',
            'left'          => 0,
            'top'           => 456,
            'width'         => 2030,
            'height'        => 445
        ]);
        BoardTemplateWidgets::firstOrCreate([
            'boardTypeId'   => 8,
            'widgetId'      => 1,
            'tagId'         => 'tagId-21',
            'left'          => 0,
            'top'           => 912,
            'width'         => 2030,
            'height'        => 445
        ]);
        BoardTemplateWidgets::firstOrCreate([
            'boardTypeId'   => 8,
            'widgetId'      => 1,
            'tagId'         => 'tagId-22',
            'left'          => 0,
            'top'           => 1372,
            'width'         => 2030,
            'height'        => 445
        ]);

        Widgets::firstOrCreate([
            "name"      => "Candle Chart",
            "active"    => "1",
            "minWidth"  => "600",
            "minHeight" => "600",
        ]);
        Widgets::firstOrCreate([
            "name"      => "Order Form",
            "active"    => "0",
            "minWidth"  => "500",
            "minHeight" => "800",
        ]);
    }
}
