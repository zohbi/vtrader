<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBoardElementPositionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('board_widget_positions', function (Blueprint $table) {
            $table->id();
            $table->integer('userId');
            $table->string('boardId');
            $table->string('widgetId');
            $table->string('tagId');
            $table->string('left');
            $table->string('top');
            $table->string('width');
            $table->string('height');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('board_widget_positions');
    }
}
