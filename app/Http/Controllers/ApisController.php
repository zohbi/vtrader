<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Boards;
use App\Models\BoardTemplateWidgets;
use App\Models\BoardTypes;
use App\Models\BoardWidgetPositions;
use App\Models\User;
use App\Models\Widgets;

class ApisController extends Controller
{
    public function changePostion(Request $request)
    {
        $data = array(
            "userId"    => Auth::user()->id,
            "id"        => $request["positionId"],
            "left"      => $request["newDimensions"]["left"],
            "top"       => $request["newDimensions"]["top"],
            "width"     => $request["newDimensions"]["width"],
            "height"    => $request["newDimensions"]["height"],
        );
        $BoardWidgetPositions = BoardWidgetPositions::where('userId', $data["userId"])
                                    ->where('id', $data["id"])
                                    // ->where('widgetId', $data["widgetId"])
                                    // ->where('tagId', $data["tagId"])
                                    ->update([
                                        "left"      => $data["left"],
                                        "top"       => $data["top"],
                                        "width"     => $data["width"],
                                        "height"    => $data["height"],
                                    ]);
        return response()->json($BoardWidgetPositions);
    }
    public function addWidget(Request $request)
    {   
        $widgets = Widgets::find($request["widgetId"]);
        $boardWidgetPositions = BoardWidgetPositions::create([
            "userId"    => Auth::user()->id,
            "boardId"   => $request["boardId"],
            "widgetId"  => $widgets->id,
            "tagId"     => "",
            "left"      => 0,
            "top"       => 0,
            "width"     => $widgets->minWidth,
            "height"    => $widgets->minHeight,
        ]);
        $tagId = 'tagId-'.$boardWidgetPositions->id;
        $boardWidgetPositions->tagId = $tagId; 
        $boardWidgetPositions->save();
        return $boardWidgetPositions;
    }

    public function deleteWidget(Request $request)
    {
        $boardWidgetPositions = BoardWidgetPositions::find($request["widgetId"]);
        if($boardWidgetPositions){
            $destroy = BoardWidgetPositions::destroy($request["widgetId"]);
        }
        return $destroy;
    }

    public function boardArrageArr()
    {
        $customizeBoardArr  = [];
        $boards             = Boards::where("userId", Auth::user()->id)->get();
        foreach ($boards as $board) {
            $widgets    = BoardWidgetPositions::where([
                ["userId",  "=", Auth::user()->id],
                ["boardId", "=", $board->id]
            ])->get();
            array_push($customizeBoardArr, [
                "id"        => $board->id,
                "title"     => $board->name,
                "widgets"   => $widgets,
                "editMode"  => false,
            ]);
        }
        return $customizeBoardArr;
    }

    public function addBoard(Request $request)
    {
        $boardTypes = BoardTypes::find($request["boardId"]);
        $board      = Boards::create(
            [
                "userId"        => Auth::user()->id,
                "boardTypeId"   => $boardTypes->id,
                "name"          => $boardTypes->name
            ]
        );
        $data = BoardTemplateWidgets::where("boardTypeId", $boardTypes->id)->get();
        foreach ($data as $row ) {
            $boardWidgetPositions = BoardWidgetPositions::create([
                "userId"    => Auth::user()->id,
                "boardId"   => $board->id,
                "widgetId"  => $row->widgetId,
                "tagId"     => $row->tagId,
                "left"      => $row->left,
                "top"       => $row->top,
                "width"     => $row->width,
                "height"    => $row->height,
            ]);
            $tagId = 'tagId-'.$boardWidgetPositions->id;
            $boardWidgetPositions->tagId = $tagId; 
            $boardWidgetPositions->save();
        }
        $widgets = BoardWidgetPositions::where([
            ["userId",  "=", Auth::user()->id],
            ["boardId", "=", $board->id]
        ])->get();
        return [
            "id"        => $board->id,
            "title"     => $board->name,
            "widgets"   => $widgets,
            "editMode"  => false,
        ];
    }

    public function removeBoard(Request $request)
    {
        Boards::where('id', $request["boardId"])->delete();
        BoardWidgetPositions::where('id', $request["boardId"])->delete();
        return ["boardId"=>$request["boardId"]];
    }

    public function editBoardName(Request $request)
    {
        $png_url    = "boardImage-".Auth::user()->id."-".time().".png";
        $boardImage = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $request["boardImage"]));
        $path       = public_path().'/usersImages/boards/'.$png_url;
        file_put_contents($path, $boardImage);
        $boardType = BoardTypes::create([
            "user_id"   => Auth::user()->id,
            "name"      => $request["name"],
            "image"     => '/usersImages/boards/'.$png_url
        ]);
        Boards::find($request["boardId"])->update(['name' => $request["name"],"boardTypeId" => $boardType->id]);
        $board_widgets = BoardWidgetPositions::where("boardId", "=",$request["boardId"])->get();

        foreach($board_widgets as $widget)
        {
            BoardTemplateWidgets::create([
                'boardTypeId' => $boardType->id,
                'widgetId' => $widget->widgetId,
                'tagId' => $widget->tagId,
                'left' => $widget->left,
                'top' => $widget->top,
                'width' => $widget->width,
                'height' => $widget->height
            ]);
        }

        return $this->boardArrageArr();

    }

    public function userTheme(Request $request)
    {
        try {
            setcookie("theme", $request->theme, time() + (86400 * 30), "/"); // 86400 = 1 day
            $user = User::where('id',Auth::user()->id)->first();
            if ($request->theme != $user->themeSetting) 
            {  
                $user->themeSetting = $request->theme;
                $user->save(); 
            }
        } 
        catch (\Throwable $th) {
            return false;
        }
        return true;
    }
}
