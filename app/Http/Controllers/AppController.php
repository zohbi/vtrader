<?php

namespace App\Http\Controllers;

use Inertia\Inertia;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Boards;
use App\Models\BoardTypes;
use App\Models\BoardWidgetPositions;
use App\Models\BoardTemplateWidgets;
use App\Models\Widgets;
use App\Models\UserLog;

class AppController extends Controller
{
    public function board()
    {
        $defaultBoards  = BoardTypes::where("user_id",  "=", 1)->whereNotIn('id', [1,2])->get();
        $userBoards     = BoardTypes::where("user_id",  "=", Auth::user()->id)->get();
        return Inertia::render('Board',[
            "activePage"        => "Trade",
            "defaultBoards"     => $defaultBoards,
            "userBoards"        => $userBoards
        ]);
    }

    public function customizeBoard(Request $request)
    {
        $customizeBoardArr = [];
        if($request->has("boardId")){
            $boardTypes = BoardTypes::find($request["boardId"]);
            $board = Boards::Create([
                "userId"        => Auth::user()->id,
                "boardTypeId"   => $boardTypes->id,
                "name"          => $boardTypes->name
            ]);
            $data = BoardTemplateWidgets::where("boardTypeId", $boardTypes->id)->get();
            foreach ($data as $row ) {
                $boardWidgetPositions = BoardWidgetPositions::create([
                    "userId"    => Auth::user()->id,
                    "boardId"   => $board->id,
                    "widgetId"  => $row->widgetId,
                    "tagId"     => $row->tagId,
                    "left"      => $row->left,
                    "top"       => $row->top,
                    "width"     => $row->width,
                    "height"    => $row->height,
                ]);
                $tagId = 'tagId-'.$boardWidgetPositions->id;
                $boardWidgetPositions->tagId = $tagId; 
                $boardWidgetPositions->save();
            }
            $widgets = BoardWidgetPositions::where([
                ["userId",  "=", Auth::user()->id],
                ["boardId", "=", $board->id]
            ])->get();
        }
        $boards     = Boards::where("userId", Auth::user()->id)->get();
        if(count($boards)==0){
            return redirect('/board');
        }
        foreach ($boards as $board) {
            $widgets    = BoardWidgetPositions::where([
                ["userId",  "=", Auth::user()->id],
                ["boardId", "=", $board->id]
            ])->get();
            if(count($widgets)==0){
                $data = BoardTemplateWidgets::where("boardTypeId", $board->id)->get();
                foreach ($data as $row ) {
                    $boardWidgetPositions = BoardWidgetPositions::create([
                        "userId"    => Auth::user()->id,
                        "boardId"   => $board->id,
                        "widgetId"  => $row->widgetId,
                        "tagId"     => $row->tagId,
                        "left"      => $row->left,
                        "top"       => $row->top,
                        "width"     => $row->width,
                        "height"    => $row->height,
                    ]);
                    $tagId = 'tagId-'.$boardWidgetPositions->id;
                    $boardWidgetPositions->tagId = $tagId; 
                    $boardWidgetPositions->save();
                }
                $widgets = BoardWidgetPositions::where([
                    ["userId",  "=", Auth::user()->id],
                    ["boardId", "=", $board->id]
                ])->get();
            }
            array_push($customizeBoardArr, [
                "id"        => $board->id,
                "title"     => $board->name,
                "widgets"   => $widgets,
                "editMode"  => false,
            ]);
        }
        $templateBoards     = BoardTypes::where("user_id",  "=", 1)->whereNotIn('id', [1,2])->get();
        $userTemplateBoards = BoardTypes::where("user_id",  "=", Auth::user()->id)->get();
        return Inertia::render('CustomizeBoard',[
            "activePage"         => "Trade",
            "userBoards"         => $customizeBoardArr,
            "widgets"            => Widgets::where("active",1)->get(),
            "templateBoard"      => $templateBoards,
            "userTemplateBoards" => $userTemplateBoards
        ]);
    }

    public function profile(){
        $userDetail = Auth::user();
        $user_log = UserLog::where('user_id',Auth::id())->latest()->first();
        // dd($user_log);
        // dd($userDetail);
        return view('pages.profile')->with(["userDetail"=>$userDetail,"user_log"=>$user_log]);
    }

    public function Watchlist(){
        
        $widgets = BoardWidgetPositions::where([
            ["userId",  "=", Auth::user()->id],
            ["boardId", "=", "2"]
        ])->get();
        if(count($widgets)==0){
            $data = BoardTemplateWidgets::where("boardTypeId", 1)->get();
            foreach ($data as $row ) {
                $boardWidgetPositions = BoardWidgetPositions::create([
                    "userId"    => Auth::user()->id,
                    "boardId"   => 2,
                    "widgetId"  => $row->widgetId,
                    "tagId"     => $row->tagId,
                    "left"      => $row->left,
                    "top"       => $row->top,
                    "width"     => $row->width,
                    "height"    => $row->height,
                ]);
                $tagId = 'tagId-'.$boardWidgetPositions->id;
                $boardWidgetPositions->tagId = $tagId; 
                $boardWidgetPositions->save();
            }
            $widgets = BoardWidgetPositions::where([
                ["userId",  "=", Auth::user()->id],
                ["boardId", "=", "2"]
            ])->get();
        }
        return Inertia::render('Watchlist',[
            "activePage"    => "Watchlist",
            "widgets"       => $widgets
        ]);
      
    }
    
    public function account(){
        return Inertia::render('Accounts',[
            "activePage"    => "Account",
        ]);
    }

    public function Orderform(){
        return Inertia::render('orderform',[
            "activePage"    => "orderform",
        ]);
    }
    
    public function MultiTab(){
        return Inertia::render('Multitab',[
            "activePage"    => "Multitab",
        ]);
    }
}
