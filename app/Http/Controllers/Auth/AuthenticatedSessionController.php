<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Boards;
use App\Models\UserLog;

class AuthenticatedSessionController extends Controller
{
    /**
     * Display the login view.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('auth.login');
    }

    /**
     * Handle an incoming authentication request.
     *
     * @param  \App\Http\Requests\Auth\LoginRequest  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(LoginRequest $request)
    {
        $request->authenticate();
        
        $request->session()->regenerate();

        $this->setUserLog(Auth::id());
        if(Boards::where('userId',Auth::id())->exists()){
            return redirect()->intended(RouteServiceProvider::CUSTOMIZE_BOARD);
        }else{
            return redirect()->intended(RouteServiceProvider::HOME);
        }
        
    }

    /**
     * Destroy an authenticated session.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Request $request)
    {
        Auth::guard('web')->logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return redirect('/');
    }

    public function setUserLog($user_id)
    {
        $os                 = $this->getOS();
        $browser            = $this->getBrowser();
        $physical_address   = "";

        if (str_contains($os, 'Windows')) 
        { 
            $physical_address   = explode(" ",exec('getmac'));
            $physical_address   = $physical_address[0];
            $ipconfig           = explode("\n",shell_exec('ipconfig'));
            $ipV6               = explode(". :",$ipconfig[8]); 
        }
        elseif(str_contains($os, 'Mac'))
        {
            $physical_address_command   = "networksetup -listallhardwareports";
            $command                    = "/sbin/ifconfig";

            exec($physical_address_command, $physical_address_output);
            $physical_address   = explode(": ",$physical_address_output[3]);
            $physical_address   = $physical_address[1];

            exec($command, $output);
            $ifconfig   = explode(" ",$output[14]);
            $ipV6       = $ifconfig[1];
        }

        $UserLog                    = new UserLog;
        $UserLog->user_id           = $user_id;
        $UserLog->ip_address        = $ipV6[1];
        $UserLog->physical_address  = $physical_address;
        $UserLog->os                = $os;
        $UserLog->browser           = $browser;
        $UserLog->save();

        return true;
    }


    public function getOS()
    {
        $user_agent = $_SERVER['HTTP_USER_AGENT'];

        $os_platform  = "Unknown OS Platform";

        $os_array     = array(
            '/windows nt 10/i'      =>  'Windows 10',
            '/windows nt 6.3/i'     =>  'Windows 8.1',
            '/windows nt 6.2/i'     =>  'Windows 8',
            '/windows nt 6.1/i'     =>  'Windows 7',
            '/windows nt 6.0/i'     =>  'Windows Vista',
            '/windows nt 5.2/i'     =>  'Windows Server 2003/XP x64',
            '/windows nt 5.1/i'     =>  'Windows XP',
            '/windows xp/i'         =>  'Windows XP',
            '/windows nt 5.0/i'     =>  'Windows 2000',
            '/windows me/i'         =>  'Windows ME',
            '/win98/i'              =>  'Windows 98',
            '/win95/i'              =>  'Windows 95',
            '/win16/i'              =>  'Windows 3.11',
            '/macintosh|mac os x/i' =>  'Mac OS X',
            '/mac_powerpc/i'        =>  'Mac OS 9',
            '/linux/i'              =>  'Linux',
            '/ubuntu/i'             =>  'Ubuntu',
            '/iphone/i'             =>  'iPhone',
            '/ipod/i'               =>  'iPod',
            '/ipad/i'               =>  'iPad',
            '/android/i'            =>  'Android',
            '/blackberry/i'         =>  'BlackBerry',
            '/webos/i'              =>  'Mobile'
        );

        foreach ($os_array as $regex => $value)
        {
            if (preg_match($regex, $user_agent))
            {
                $os_platform = $value;
            }
        }

        return $os_platform;
    }

    public function getBrowser() 
    {
        $user_agent = $_SERVER['HTTP_USER_AGENT'];
    
        $browser        = "Unknown Browser";
    
        $browser_array = array(
                                '/msie/i'      => 'Internet Explorer',
                                '/firefox/i'   => 'Firefox',
                                '/safari/i'    => 'Safari',
                                '/chrome/i'    => 'Chrome',
                                '/edge/i'      => 'Edge',
                                '/opera/i'     => 'Opera',
                                '/netscape/i'  => 'Netscape',
                                '/maxthon/i'   => 'Maxthon',
                                '/konqueror/i' => 'Konqueror',
                                '/mobile/i'    => 'Handheld Browser'
                         );
    
        foreach ($browser_array as $regex => $value)
        {
            if (preg_match($regex, $user_agent))
            {
                $browser = $value;
            }
        }
            
    
        return $browser;
    }
}
