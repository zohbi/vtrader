<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use App\Mail\GetVerificationCodeMail;
use App\Models\VerifyEmailCode;
class RegisteredUserController extends Controller
{
    /**
     * Display the registration view.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('auth.register');
    }

    /**
     * Handle an incoming registration request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $check = VerifyEmailCode::where([['verify_code','=',$request->verify_code],['email','=',$request->email]])->first();
        if($check != null)
        {

            $request->validate([
                'name'      => 'required|string|max:255',
                'email'     => 'required|string|email|max:255|unique:users',
                'password'  => 'required|string|confirmed|min:8',
            ]);
    
            Auth::login($user = User::create([
                'name'          => $request->name,
                'email'         => $request->email,
                'password'      => Hash::make($request->password),
                'userTypeId'    => "2"
            ]));
                
            VerifyEmailCode::where([['email','=',$request->email]])->delete();

            event(new Registered($user));
    
            return redirect(RouteServiceProvider::HOME);
        }
        else{
            // return redirect()->back()->with('error','Woops!,There is some issue please try again later.');
            return redirect()->back()->with('error','Invalid Verification Code.');

        }
        
    }

    public function AjaxCallForEmailVerificationCode(Request $request)
    {
        $generate_verify_code = rand(100000,999999);

        $User = User::where('email',$request->email)->first();
        $details = array();
        if ($User == null) 
        {
            $check = VerifyEmailCode::where('verify_code',$generate_verify_code)->first();
            try {
                $VerifyEmailCode                = new VerifyEmailCode;
                $VerifyEmailCode->verify_code   = $generate_verify_code;
                $VerifyEmailCode->email         = $request->email;
                $VerifyEmailCode->save();

                Mail::to($request->email)->send(new GetVerificationCodeMail($generate_verify_code,$request->email));

                
            } catch (\Throwable $th) {

                $details[0] = false;
                $details[1] = 'email-send-error';
                $details[2] = 'Woops!,There is some issue please try again later.';
                return $details;
            }
        }
        else{
            $details[0] = false;
            $details[1] = 'email-error';
            $details[2] = 'Email is Already Used!';
            return $details;
        }

        

        $details[0] = true;
        $details[1] = 'email-send-success';
        $details[2] = 'Verification code has been sent!';
        return $details;
    }
}
