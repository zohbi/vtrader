<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Mail\ForgotPasswordEmail;
use Illuminate\Support\Facades\Mail;
use App\Models\User;
use Auth;
use Hash;
use Illuminate\Support\Facades\Redirect;


class ForgetPasswordResetController extends Controller
{
    public function AjaxCallForForgotPasswordEmail(Request $request)
    {
        // dd($request->all());
        $to_email       = $request->email;
        $email_encode   = $to_email;
        for ($i=0; $i < 3; $i++) { 
            $email_encode = base64_encode($email_encode);
        }
        $detail = array();
        $User  = User::where('email',$to_email)->first();
        // dd($User);
        if($User != null)
        {
            Mail::to($to_email)->send(new ForgotPasswordEmail($email_encode));
            $detail[0] = true;
            $detail[1] = "Email is Succesfully send!";
            return $detail;
        }
        else{
            $detail[0] = false;
            $detail[1] = "Invalid Email!";
            return $detail;
        }
    }

    public function ResetPassword($email_encode)
    {
        $email_decode = $email_encode;
        for ($i=0; $i < 3; $i++) { 
            $email_decode = base64_decode($email_decode);
        }
        return view('auth.passwords.resetpassword',compact('email_decode'));
    }

    public function SetNewPassword(Request $request)
    {
        $request->validate([
            'new_password'              => 'min:8|required_with:confirm_new_password|same:confirm_new_password',
            'confirm_new_password'      => 'min:8',
        ],[
            'new_password.required'              => 'This field is required',
            'confirm_new_password.required'      => 'This field is required',
        ]);

        try {
            
            $User  = User::where('email',$request->email)->first();

            if($User != null)
            {
                $User->password = Hash::make($request->new_password);
                $User->save();
            }
            
        } 
        catch (\Throwable $th) {
            return redirect()->back()->with('message',$th);
        }
        return Redirect::to('/login')->with('success','Your password reset successfully!');
    }
}
