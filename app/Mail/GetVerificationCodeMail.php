<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class GetVerificationCodeMail extends Mailable
{
    use Queueable, SerializesModels;

    public $verify_code;
    public $email;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($verify_code,$email)
    {
        $this->verify_code  = $verify_code;
        $this->email        = $email;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from(env('MAIL_USERNAME'))->view('email.registerVerificationCodeEmail')->with(['verify_code' => $this->verify_code, 'email' => $this->email]);
    }
}
