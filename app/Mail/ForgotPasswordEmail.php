<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ForgotPasswordEmail extends Mailable
{
    use Queueable, SerializesModels;
    public $email_encode = "";

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($email_encode)
    {
        $this->email_encode = $email_encode;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from(env('MAIL_USERNAME'))->view('email.forgotPasswordEmail')->with('email_decode',$this->email_encode);
    }
}
