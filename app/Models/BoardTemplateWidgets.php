<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BoardTemplateWidgets extends Model
{
    use HasFactory;
    protected $fillable = [
        'boardTypeId',
        'widgetId',
        'tagId',
        'left',
        'top',
        'width',
        'height'
    ];
}
