<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BoardWidgetPositions extends Model
{
    use HasFactory;
    protected $fillable = [
        'userId',
        'boardId',
        'widgetId',
        'tagId',
        'left',
        'top',
        'width',
        'height'
    ];

}
