@extends('layouts.guest')

@push('styles')
    <title>{{@$termsAndServices->title}}</title>
    <meta name="description" content="{{@$termsAndServices->description}}">
    <link href="{{asset('assets/css/login.css')}}" rel="stylesheet">
    <style>
        .forget-pass-bg {
            background-image: url(/assets/img/forget-pass-bg.png);
            background-position: center center;
            background-repeat: no-repeat;
            background-size: 100% 100%;
            border-radius: 20px;
            width: 80%;
            -webkit-box-shadow: 4px -1px 13px -3px rgba(0,0,0,0.93);
            -moz-box-shadow: 4px -1px 13px -3px rgba(0,0,0,0.93);
            box-shadow: 4px -1px 13px -3px rgba(0,0,0,0.93);
        }
    </style>
@endpush

@section('content')

    @include('components.darklight')
    <div class="container-fluid pd-0 login-main-bg">
        @include('components.nav-link')
        <div class="login-bg">
            <div class="container">
                <div class="row">
                    <div class="col-md-5">
                        <div id="content">
                            <h1 class="h1-style">Welcome to <br /><span class="h1-bold">Vtrader</span></h1>
                        </div>
                    </div>
                    <div class="col-md-7">
                        <div class="forget-pass-bg">
                            <div style="padding: 50px;">
                                <h4 style="color: white;background-color: #000c138a;padding: 10px 5px;font-size: 17px;">Enter New Password</h4>
                                <form method="POST" action="{{route('SetNewPassword')}}">
                                @csrf
                                    <div class="input-group">
                                        <span>
                                            <img src="{{asset('assets/img/login-id.png')}}" />
                                        </span>
                                        <input type="email" class="form-control text-box" placeholder="Email Address" name="email" value="{{$email_decode}}" readonly/>
                                    </div>
                                    <div class="input-group">
                                        <span>
                                            <img src="{{asset('assets/img/login-password.png')}}"
                                                style="padding-top: 5px;" />
                                        </span>
                                        <input type="password" class="form-control text-box" placeholder="Enter Password" id="" name="new_password" required autocomplete="current-password" />
                                    </div>
                                    @error('new_password')
                                    <p class="text-danger text-sm">{{$message}}</p>
                                    @enderror
                                    <div class="input-group">
                                        <span>
                                            <img src="{{asset('assets/img/login-password.png')}}"
                                                style="padding-top: 5px;" />
                                        </span>
                                        <input type="password" class="form-control text-box" placeholder="Verify Password" id="" name="confirm_new_password" required autocomplete="current-password" />
                                    </div>
                                    @error('confirm_new_password')
                                    <p class="text-danger text-sm">{{$message}}</p>
                                    @enderror
                                    <div style="text-align: center;">
                                        <button type="submit" class="login-btn-one">Confirm</button>
                                    </div>
                                </form>
                            </div>  
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @include('components.footer')
    </div>
@endsection
