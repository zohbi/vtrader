@extends('layouts.guest')

@push('styles')
    <title>{{@$termsAndServices->title}}</title>
    <meta name="description" content="{{@$termsAndServices->description}}">
    <link href="{{asset('assets/css/login.css')}}" rel="stylesheet">

    <style>

        @keyframes spinner-border {
            to { transform: rotate(360deg); }
        }

        .spinner-border {
            position: relative;
            display: inline-block;
            width: 2rem;
            height: 2rem;
            overflow: hidden;
            text-indent: -999em;
            vertical-align: text-bottom;
            border: .25em solid;
            border-color: currentColor transparent currentColor currentColor;
            border-radius: 50%;
            animation-name: spinner-border;
            animation-duration: .75s;
            animation-timing-function: linear;
            animation-iteration-count: infinite;
        }

        .spinner-border-reverse {
            border-color: transparent currentColor transparent transparent;

            &::after {
                position: absolute;
                top: -.25em;
                right: -.25em;
                bottom: -.25em;
                left: -.25em;
                display: inline-block;
                content: "";
                border: .25em solid rgba(0, 0, 0, .1);
                border-radius: 50%;
            }
        }

        .spinner-border-sm {
            width: 1rem;
            height: 1rem;
            border-width: .2em;

            &.spinner-border-reverse::after {
                border-width: .2em;
            }
        }


        @keyframes spinner-grow {
            0% {
                opacity: 0;
                transform: scale(0);
            }
            50% {
                opacity: 1;
            }
            100% {
                opacity: 0;
                transform: scale(1);
            }
        }

        .spinner-grow {
        position: relative;
        display: inline-block;
        width: 2rem;
        height: 2rem;
        overflow: hidden;
        text-indent: -999em;
        vertical-align: text-bottom;
        background-color: currentColor;
        border-radius: 50%;
        animation-name: spinner-grow;
        animation-duration: .75s;
        animation-timing-function: linear;
        animation-iteration-count: infinite;
        }

        .spinner-grow-sm {
        width: 1rem;
        height: 1rem;
        }
        .forget-pass-bg {
            background-image: url(/assets/img/forget-pass-bg.png);
            background-position: center center;
            background-repeat: no-repeat;
            background-size: 100% 100%;
            border-radius: 20px;
            width: 80%;
            -webkit-box-shadow: 4px -1px 13px -3px rgba(0,0,0,0.93);
            -moz-box-shadow: 4px -1px 13px -3px rgba(0,0,0,0.93);
            box-shadow: 4px -1px 13px -3px rgba(0,0,0,0.93);
        }
    </style>

@endpush

@section('content')

    @include('components.darklight')
    <div class="container-fluid pd-0 login-main-bg">
        @include('components.nav-link')
        <div class="login-bg">
            <div class="container">
                <div class="row">
                    <div class="col-md-5">
                        <div id="content">
                            <h1 class="h1-style">Welcome to <br /><span class="h1-bold">Vtrader</span></h1>
                        </div>
                    </div>
                    <div class="col-md-7">
                        <div class="forget-pass-bg">
                            <div style="padding: 50px;">
                                <h4 style="font-size: 17px;">Forget Password?</h4>
                                <form method="POST" action="#">
                                    <div id="success_msg"></div>
                                    <div id="error_msg"> </div>
                                @csrf
                                    <div class="input-group">
                                        <span>
                                            <img src="{{asset('assets/img/login-id.png')}}" />
                                        </span>
                                        <input type="email" id="email" name="email" class="form-control text-box" placeholder="Email Address" autofocus />
                                    </div>
                                    @error('email')
                                        <p class="mt-4 text-xs italic text-red-500">
                                            {{ $message }}
                                        </p>
                                    @enderror
                                    <div class="row" id="loader" style="display: none">
                                        <div class="col-12 d-flex justify-content-center">
                                            <div class="spinner-border" role="status">
                                                <span class="sr-only">Loading...</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div style="text-align: center;">
                                        <button type="submit" class="login-btn-one" id="btn_email">Send Email</button>
                                    </div>
                                </form>
                            </div>  
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @include('components.footer')
    </div>
@endsection

@push('scripts')
<script>
    $('#btn_email').click(function (e) {
        e.preventDefault(); 
        let email = $('#email').val();

        if(email != "")
        {
            $.ajax({
                type: "POST",
                url: "{{route('AjaxCallForForgotPasswordEmail')}}",
                data: {
                    _token: '{{ csrf_token() }}',
                    email: email,
                },
                beforeSend: function () {
                    $('#loader').show();
                },
                success: function (response) {

                    if (response[0]) {
                        // success_msg
                        $('#loader').hide();
                        $('#success_msg').append(`<p class="text-success">` + response[1] + `</p>`);
                    } else {
                        $('#loader').hide();
                        // error_msg
                        $('#error_msg').append(`<p class="text-danger">` + response[1] + `</p>`);

                    }
                }

            });
        }
        else{
            $('#email').focus();
            $('#error_msg').show()
            $('#error_msg').text("");
            $('#error_msg').append(`<p class="text-danger">Email field is empty, Please enter the valid email!</p>`);

            setTimeout(function(){$('#error_msg').hide()},5000);
        }
        

    })

</script>
@endpush
