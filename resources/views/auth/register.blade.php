@extends('layouts.guest')
@push('styles')
    {{-- <title>{{@$termsAndServices->title}}</title> --}}
    <title>VTrader | Register</title>
    <meta name="description" content="{{@$termsAndServices->description}}">
    <link href="{{asset('assets/css/signup.css')}}" rel="stylesheet">

    <style>

        @keyframes spinner-border {
            to { transform: rotate(360deg); }
        }

        .spinner-border {
            position: relative;
            display: inline-block;
            width: 2rem;
            height: 2rem;
            overflow: hidden;
            text-indent: -999em;
            vertical-align: text-bottom;
            border: .25em solid;
            border-color: currentColor transparent currentColor currentColor;
            border-radius: 50%;
            animation-name: spinner-border;
            animation-duration: .75s;
            animation-timing-function: linear;
            animation-iteration-count: infinite;
        }

        .spinner-border-reverse {
            border-color: transparent currentColor transparent transparent;

            &::after {
                position: absolute;
                top: -.25em;
                right: -.25em;
                bottom: -.25em;
                left: -.25em;
                display: inline-block;
                content: "";
                border: .25em solid rgba(0, 0, 0, .1);
                border-radius: 50%;
            }
        }

        .spinner-border-sm {
            width: 1rem;
            height: 1rem;
            border-width: .2em;

            &.spinner-border-reverse::after {
                border-width: .2em;
            }
        }


        @keyframes spinner-grow {
            0% {
                opacity: 0;
                transform: scale(0);
            }
            50% {
                opacity: 1;
            }
            100% {
                opacity: 0;
                transform: scale(1);
            }
        }

        .spinner-grow {
        position: relative;
        display: inline-block;
        width: 2rem;
        height: 2rem;
        overflow: hidden;
        text-indent: -999em;
        vertical-align: text-bottom;
        background-color: currentColor;
        border-radius: 50%;
        animation-name: spinner-grow;
        animation-duration: .75s;
        animation-timing-function: linear;
        animation-iteration-count: infinite;
        }

        .spinner-grow-sm {
        width: 1rem;
        height: 1rem;
        }
    </style>
@endpush
@section('content')


@include('components.darklight')

    <div class="container-fluid pd-0 login-main-bg">
        @include('components.nav-link')
        <div class="login-bg">
            <div class="container">
                <div class="row">
                    <div class="col-md-5">
                        <div id="content">
                            <h1 class="h1-style">Welcome to <br /><span class="h1-bold">Vtrader</span></h1>
                        </div>
                    </div>
                    <div class="col-md-7">
                        <div class="login-box">
                            
                            <div class="tab-box">
                                
                                @if (Session::has('success'))
                                    {{-- <h3 style="color: red">{{Session::get('success')}}</h3> --}}
                                    <div class="alert alert-success alert-dismissible">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong>Success!</strong> {{Session::get('success')}}
                                    </div>
                                @elseif(Session::has('error'))
                                    {{-- <h3 style="color: red">{{Session::get('error')}}</h3> --}}
                                    <div class="alert alert-danger alert-dismissible">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong>Danger!</strong> {{Session::get('error')}}
                                      </div>
                                @endif

                                <div class="tabs">
                                    <button class="tabs__button tabs__button--active" type="button">Email Sign up</button>
                                    <button class="tabs__button" type="button">Mobile Sign up</button>
                                </div>

                                <div class="tab-content tab-content--active">
                                    <form method="POST" action="{{ route('register') }}">
                                        @csrf
                                        <div class="input-group">
                                            <span>
                                                <img src="{{asset('assets/img/login-id.png')}}" />
                                            </span>
                                            <input type="text" class="form-control text-box" name="name" placeholder="Full Name" value="{{old('name')}}" required />
                                        </div>
                                        <div class="input-group">
                                            <span>
                                                <img src="{{asset('assets/img/login-id.png')}}" />
                                            </span>
                                            <input type="email" class="form-control text-box" name="email" placeholder="Email-Address" value="{{old('email')}}" id="email" required />
                                        </div>
                                        @error('email')
                                        <p class="text-danger text-sm">{{$message}}</p>
                                        @enderror
                                        <div class="input-group">
                                            <span>
                                                <img src="{{asset('assets/img/login-password.png')}}"
                                                    style="padding-top: 5px;" />
                                            </span>
                                            <input type="password" class="form-control text-box"
                                                name="password"
                                                placeholder="Password (8-20 digits)" required />
                                        </div>
                                        @error('password')
                                            <p class="text-danger text-sm">{{$message}}</p>
                                        @enderror
                                        <div class="input-group">
                                            <span>
                                                <img src="{{asset('assets/img/login-password.png')}}"
                                                    style="padding-top: 5px;" />
                                            </span>
                                            <input type="password" class="form-control text-box"
                                                name="password_confirmation"
                                                placeholder="Confirm Password" required />
                                        </div>
                                        @error('password_confirmation')
                                            <p class="text-danger text-sm">{{$message}}</p>
                                        @enderror
                                        <div class="input-group">
                                            <span>
                                                <img src="{{asset('assets/img/verify.png')}}" style="padding-top: 5px;" />
                                            </span>
                                            <input type="text" class="form-control text-box"
                                                placeholder="Verification Code" name="verify_code" required />
                                            <span>
                                                <button name="getcode" type="button" class="getcode-btn" id="get_code">(Get Code)</button>
                                                <div class="clearfix verify-spinner-loading" style="display: none">
                                                    <div class="spinner-border float-right" role="status">
                                                    <span class="sr-only">Loading...</span>
                                                    </div>
                                                </div>
                                            </span>
                                            
                                        </div>
                                        
                                        {{-- <p class="verify_code_msg text-danger" style="color: #1fff1f;text-align:center;">dsadasdas</p> --}}

                                        <p class="verify_code_msg" style="color: #1fff1f;text-align:center;display:none"></p>

                                        <div style="text-align: center;margin-top: 80px;">
                                            <button type="submit" class="login-btn-one">Sign Up</button>
                                        </div>
                                        <div>
                                            <p class="signup-means">Sign up means you agree to Terms of Service and Privacy
                                                Policy</p>
                                        </div>
                                        <div class="mrg-top-txt-center">
                                            <p class="sign-in-opt">Already have an account? <strong><a
                                                        href="/login"
                                                        style="color:white;text-decoration:none;">Log in</a></strong></p>
                                        </div>
                                        <div class="txt-center">
                                            <a href="#"><img src="{{asset('assets/img/gmail-icon.png')}}"
                                                    class="login-icon" /></a>
                                            <a href="#" style="margin-left:30px;"><img
                                                    src="{{asset('assets/img/facebook-white.png')}}"
                                                    class="login-icon" /></a>
                                        </div>
                                    </form>
                                </div>

                                <div class="tab-content">
                                    <form method="POST" action="#">
                                        @csrf
                                        <div class="input-group">
                                            <span>
                                                <img src="{{asset('assets/img/login-id.png')}}" />
                                            </span>
                                            <input type="number" class="form-control text-box" placeholder="Mobile No." />
                                        </div>
                                        <div class="input-group">
                                            <span>
                                                <img src="{{asset('assets/img/login-password.png')}}"
                                                    style="padding-top: 5px;" />
                                            </span>
                                            <input type="password" class="form-control text-box"
                                                name="password"
                                                placeholder="Password (8-20 digits)" />
                                        </div>
                                        <div class="input-group">
                                            <span>
                                                <img src="{{asset('assets/img/login-password.png')}}"
                                                    style="padding-top: 5px;" />
                                            </span>
                                            <input type="password" class="form-control text-box"
                                                name="password_confirmation"
                                                placeholder="Confirm Password" />
                                        </div>
                                        <div class="input-group">
                                            <span>
                                                <img src="{{asset('assets/img/verify.png')}}" style="padding-top: 5px;" />
                                            </span>
                                            <input type="text" class="form-control text-box"
                                                placeholder="Verification Code" name="verify_code" />
                                            <span>
                                            <button name="getcode" type="button" class="getcode-btn">(Get Code)</button>
                                            </span>
                                        </div>
                                        <div style="text-align: center;margin-top: 80px;">
                                            <button type="submit" class="login-btn-one">Sign Up</button>
                                        </div>
                                        <div>
                                            <p class="signup-means">Sign up means you agree to Terms of Service and Privacy
                                                Policy</p>
                                        </div>
                                        <div class="mrg-top-txt-center">
                                            <p class="sign-in-opt">Already have an account? <strong><a
                                                        href="#"
                                                        style="color:white;text-decoration:none;">Log in</a></strong></p>
                                        </div>
                                        <div class="txt-center">
                                            <a href="#"><img src="{{asset('assets/img/gmail-icon.png')}}"
                                                    class="login-icon" /></a>
                                            <a href="#" style="margin-left:30px;"><img
                                                    src="{{asset('assets/img/facebook-white.png')}}"
                                                    class="login-icon" /></a>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- <img src="{{asset('assets/img/login-box.png')}}" class="">	 -->
                    </div>
                </div>
            </div>
        </div>
        @include('components.footer')
    </div>
@endsection
@push('scripts')
<script type="text/javascript">
    const btns = document.querySelectorAll(".tabs__button");
    const tabContent = document.querySelectorAll(".tab-content");

    for (let i = 0; i < btns.length; i++) {
        btns[i].addEventListener("click", () => {
            addClassFunc(btns[i], "tabs__button--active");
            clearClassFunc(i, btns, "tabs__button--active");

            addClassFunc(tabContent[i], "tab-content--active");
            clearClassFunc(i, tabContent, "tab-content--active");
        });
    }

    function addClassFunc(elem, elemClass) {
        elem.classList.add(elemClass);
    }

    function clearClassFunc(indx, elems, elemClass) {
        for (let i = 0; i < elems.length; i++) {
            if (i === indx) {
                continue;
            }
            elems[i].classList.remove(elemClass);
        }
    }

    $(".getcode-btn").click(function (e) { 

        e.preventDefault();
        
        let email = $("#email").val();
        if(email != '')
        {
            let _this = $(this);

            _this.hide();

            _this.closest('span').find('.verify-spinner-loading').show();

            // _this.attr("disabled", true);
            $.ajax({
                type: "POST",
                url: "{{ url('/api/sendCode') }}",
                data: { _token: "{{ csrf_token() }}", email:email},
                success: function (response) {

                    
                    console.log(response[0]);
                    if (response[0]) 
                    {
                        _this.closest('form').find('.verify_code_msg').show();
                        _this.closest('form').find('.verify_code_msg').text(response[2]);
                        _this.closest('form').find('.verify_code_msg').removeClass('text-danger');

                    } 
                    else{
                        _this.closest('form').find('.verify_code_msg').show();
                        _this.closest('form').find('.verify_code_msg').text(response[2]);
                        _this.closest('form').find('.verify_code_msg').addClass('text-danger');
                    }

    
                    // $('.verify_code_msg').show();
                    _this.show();
                    _this.closest('span').find('.verify-spinner-loading').hide();
                    // _this.attr("disabled", false);

                }
            });
        }
        
    });

</script>
@endpush
