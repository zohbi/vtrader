@extends('layouts.guest')

@push('styles')
    {{-- <title>{{@$termsAndServices->title}}</title> --}}
    <title>VTrader | Login</title>
    <meta name="description" content="{{@$termsAndServices->description}}">
    <link href="{{asset('assets/css/login.css')}}" rel="stylesheet">
@endpush

@section('content')

    @include('components.darklight')
    <div class="container-fluid pd-0 login-main-bg">
        @include('components.nav-link')
        <div class="login-bg">
            <div class="container">
                <div class="row">
                    <div class="col-md-5">
                        <div id="content">
                            <h1 class="h1-style">Welcome to <br /><span class="h1-bold">Vtrader</span></h1>
                        </div>
                    </div>
                    <div class="col-md-7">
                        <div class="login-box">
                            <div class="tab-box">
                                @if (Session::has('success'))
                                    {{-- <h3 style="color: red">{{Session::get('success')}}</h3> --}}
                                    <div class="alert alert-success alert-dismissible">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong>Success!</strong> {{Session::get('success')}}
                                    </div>
                                @elseif(Session::has('error'))
                                    {{-- <h3 style="color: red">{{Session::get('error')}}</h3> --}}
                                    <div class="alert alert-danger alert-dismissible">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <strong>Error!</strong> {{Session::get('error')}}
                                      </div>
                                @endif
                                <div class="tabs">
                                    <button class="tabs__button tabs__button--active" type="button">Email Login</button>
                                    <button class="tabs__button" type="button">Mobile Login</button>
                                </div>

                                <div class="tab-content tab-content--active">
                                    <form method="POST" action="{{ route('login') }}">
                                        @csrf
                                        <div class="input-group">
                                            <span>
                                                <img src="{{asset('assets/img/login-id.png')}}" />
                                            </span>
                                            <input id="email" class="form-control text-box" type="email" placeholder="Email-Address" name="email" :value="old('email')" required autofocus/>
                                        </div>
                                        <div class="input-group">
                                            <span>
                                                <img src="{{asset('assets/img/login-password.png')}}"
                                                    style="padding-top: 5px;" />
                                            </span>
                                            <input type="password" class="form-control text-box" placeholder="Password" id="password" name="password" required autocomplete="current-password" />
                                        </div>
                                        @error('email')
                                        <p class="text-danger text-sm">{{$message}}</p>
                                        @enderror
                                        <div>
                                            <a href="{{route('forget.email')}}" class="forget-pass">Forget Password</a>
                                        </div>
                                        <div style="text-align: center;margin-top: 120px;">
                                            <button type="submit" class="login-btn-one">Login</button>
                                        </div>
                                        <div class="mrg-top-txt-center">
                                            <p class="sign-in-opt"><strong>Sign up</strong> or <strong>Log in</strong> with </p>
                                        </div>
                                        <div class="mrg-top-txt-center">
                                            <a href="{{ url('auth/google') }}"><img src="{{asset('assets/img/gmail-icon.png')}}"
                                                    class="login-icon" /></a>
                                            <a href="{{ url('auth/facebook') }}" style="margin-left:30px;"><img
                                                    src="{{asset('assets/img/facebook-white.png')}}"
                                                    class="login-icon" /></a>
                                        </div>
                                    </form>
                                </div>

                                <div class="tab-content">
                                    <div>
                                        <form method="POST" action="#">
                                        @csrf
                                            <div class="input-group">
                                                <span>
                                                    <img src="{{asset('assets/img/login-id.png')}}" />
                                                </span>
                                                <input type="number" class="form-control text-box" placeholder="Mobile No." />
                                            </div>
                                            <div class="input-group">
                                                <span>
                                                    <img src="{{asset('assets/img/login-password.png')}}"
                                                        style="padding-top: 5px;" />
                                                </span>
                                                <input type="password" class="form-control text-box" placeholder="Password" id="password" name="password" required autocomplete="current-password" />
                                            </div>
                                            <div>
                                                <a href="" class="forget-pass">Forget Password</a>
                                            </div>
                                            <div style="text-align: center;margin-top: 120px;">
                                                <button type="submit" class="login-btn-one">Login</button>
                                            </div>
                                            <div class="mrg-top-txt-center">
                                                <p class="sign-in-opt"><strong>Sign up</strong> or <strong>Log in</strong> with
                                                </p>
                                            </div>
                                            <div class="mrg-top-txt-center">
                                                <a href="{{ url('auth/google') }}"><img src="{{asset('assets/img/gmail-icon.png')}}"
                                                        class="login-icon" /></a>
                                                <a href="{{ url('auth/facebook') }}" style="margin-left:30px;"><img
                                                        src="{{asset('assets/img/facebook-white.png')}}"
                                                        class="login-icon" /></a>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- <img src="{{asset('assets/img/login-box.png')}}" class="">	 -->
                    </div>
                </div>
            </div>
        </div>
        @include('components.footer')
    </div>
@endsection

@push('scripts')
    <script type="text/javascript">
        const btns = document.querySelectorAll(".tabs__button");
        const tabContent = document.querySelectorAll(".tab-content");

        for (let i = 0; i < btns.length; i++) {
            btns[i].addEventListener("click", () => {
                addClassFunc(btns[i], "tabs__button--active");
                clearClassFunc(i, btns, "tabs__button--active");

                addClassFunc(tabContent[i], "tab-content--active");
                clearClassFunc(i, tabContent, "tab-content--active");
            });
        }

        function addClassFunc(elem, elemClass) {
            elem.classList.add(elemClass);
        }

        function clearClassFunc(indx, elems, elemClass) {
            for (let i = 0; i < elems.length; i++) {
                if (i === indx) {
                    continue;
                }
                elems[i].classList.remove(elemClass);
            }
        }
    </script>
@endpush
