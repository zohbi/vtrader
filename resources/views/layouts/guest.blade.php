<!DOCTYPE html>
<html>

<head>
    <meta charset="windows-1252">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="{{ asset('assets/css/4.1.1/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/font-awesome/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/custom.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/dynamic-combine.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/toggle-btn.css') }}">
    @stack('styles')
    <title>VTrader | {{ (isset($title)? @$title:'') }}</title>
    <style>
        a.dropdown-item,
        button.dropdown-item {
            color: #fff;
        }

        button#dropdownMenuButton {
            border-radius: 50%;
            background: transparent;
            border: transparent;
        }

        .dropdown-menu.show {
            background: #000c13;
            border: 2px solid #00ada2;
            border-radius: 10px;
            left: -125px;
        }

        li.nav-items a:hover,
        li.nav-items button:hover {
            color: #42fff6 !important;
            font-weight: 400 !important;
            text-decoration: none;
            background-color: transparent;
        }

        .dropdown-item:hover {
            background-color: transparent;
            color: #42fff6 !important;
        }

    </style>
</head>

<body>
    @yield('content')
    <script src="{{ asset('assets/js/3.2.1/jquery.min.js') }}"></script>
    <script src="{{ asset('assets/js/4.1.1/bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/js/custom.js') }}"></script>
    <script src="{{ asset('assets/js/custom_jquery.js')}}"></script>

    <script>
        $(document).ready(function () {
            $('body').toggleClass(getCookie("theme"));
        });

        function getCookie(cname) {
            var name = cname + "=";
            var decodedCookie = decodeURIComponent(document.cookie);
			console.log(document.cookie)
            var ca = decodedCookie.split(';');
            for (var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ') {
                    c = c.substring(1);
                }
                if (c.indexOf(name) == 0) {
                    return c.substring(name.length, c.length);
                }
            }
            return "";
        }

        function setCookie(cname, cvalue, exdays) {
            var d = new Date();
            d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
            var expires = "expires=" + d.toGMTString();
            document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
        }

        $('.can-toggle__switch').on('click', function () {
            let theme = '';
            if ($('body').hasClass('lite-theme')) {
                theme = 'lite-theme';
                setCookie("theme", theme, 30);
            } else {
                theme = '';
                setCookie("theme", theme, 30);

            }
        });

    </script>
    @stack('scripts')
</body>

</html>
