<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'VTrader') }}</title>

        <!-- Fonts -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap">

        <!-- Styles -->
        <link rel="stylesheet" href="{{   mix('css/app.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/css/4.1.1/bootstrap.min.css') }}" >
        <link rel="stylesheet" href="{{ asset('assets/font-awesome/css/font-awesome.min.css') }}" >
        <link rel="stylesheet" href="{{ asset('assets/css/custom.css') }}" >
        <link rel="stylesheet" href="{{ asset('assets/css/pages.css')}}">
        <!-- Scripts -->
        {{-- @routes --}}
        <script src="{{ mix  ('js/app.js') }}" defer></script>
        <script src="{{ asset('assets/js/3.2.1/jquery.min.js') }}"></script>
        <script src="{{ asset('assets/js/4.1.1/bootstrap.min.js') }}"></script>
        <script src="{{ asset('assets/js/custom.js') }}"></script>
    </head>
    <body class="font-sans antialiased trade-pg trade-draggable {{@Auth::user()->themeSetting}}" >
        @inertia

        <script>
            // $(document).ready(function () {
            //     $('body').toggleClass(getCookie("theme"));
            // });
    
            // function getCookie(cname) {
            //     var name = cname + "=";
            //     var decodedCookie = decodeURIComponent(document.cookie);
            //     console.log(document.cookie)
            //     var ca = decodedCookie.split(';');
            //     for (var i = 0; i < ca.length; i++) {
            //         var c = ca[i];
            //         while (c.charAt(0) == ' ') {
            //             c = c.substring(1);
            //         }
            //         if (c.indexOf(name) == 0) {
            //             return c.substring(name.length, c.length);
            //         }
            //     }
            //     return "";
            // }
    
            // function setCookie(cname, cvalue, exdays) {
            //     var d = new Date();
            //     d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
            //     var expires = "expires=" + d.toGMTString();
            //     document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
            // }
    
            // $('.can-toggle__switch').on('click', function () {
            //     let theme = '';
            //     if ($('body').hasClass('lite-theme')) {
            //         theme = 'lite-theme';
            //         setCookie("theme", theme, 30);
            //     } else {
            //         theme = '';
            //         setCookie("theme", theme, 30);
    
            //     }
            // });
    
        </script>
    </body>
</html>
