@extends('layouts.guest')
@push('styles')
    <title>Profile</title>
    <meta name="description" content="{{@$termsAndServices->description}}">
    <link href="{{asset('assets/css/account-sidebar.css')}}" rel="stylesheet">
    <!-- Font Awesome JS -->
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/solid.js"> </script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/fontawesome.js"> </script>
@endpush
@section('content')
    <div class="container-fluid pd-0" style="background-color:#000e14;">
        <div class="main-bg">
            <div class="wrapper">
                <!-- Sidebar Holder -->
                @include('components.profile_sidebar')

                <!-- Page Content Holder -->
                <div id="content">

                    @include('components.profile_navbar')
                    <div>
                        <div class="about-profile container">
                            <h2 class="clr-white">My Account</h2>
                            <div class="row">
                                <div class="col-md-4">
                                    <img class="user-img-responsive" src="{{asset('assets/img/acc-1.png')}}">
                                </div>
                                <div class="col-md-4">
                                </div>
                                <div class="col-md-4">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            @include('components.footer')
        </div>
    </div>
@endsection
@push('scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#sidebarCollapse').on('click', function () {
                $('#sidebar').toggleClass('active');
                $(this).toggleClass('active');
            });
        });
        $(document).ready(function () {
            $('body').addClass("profile-pg");
        })

    </script>
@endpush
