@extends('layouts.guest')
@push('styles')
    <title>Profile</title>
    <meta name="description" content="{{@$termsAndServices->description}}">
    <link href="{{asset('assets/css/profile-sidebar.css')}}" rel="stylesheet">

    <!-- Font Awesome JS -->
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/solid.js"> </script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/fontawesome.js"> </script>
@endpush
@section('content')
    <div class="container-fluid pd-0" style="background-color:#000e14;">
        <div class="main-bg">
            <div class="wrapper">
                <!-- Sidebar Holder -->
                @include('components.profile_sidebar')

                <!-- Page Content Holder -->
                <div id="content">

                    @include('components.profile_navbar')
                    <div>
                        <div class="about-profile container">
                            <div class="profile-inner-row-1 row">
                                <div class="about-profile-left col-sm">
                                    <h1 style="text-transform: captilize">{{ $userDetail->name }}</h1>
                                    <p><span>Nick Name: </span><span>{{ $userDetail->name }}</span></p>
                                    <p><span>Phone Number: </span><span>+123-456-7890</span></p>
                                    <p><span>Email: </span><span>{{ $userDetail->email }}</span></p>
                                    <p><span>Total: </span><span>$0.00</span></p>
                                    <p><span>24 hours withdrawl limit: </span><span>1BTC</span></p>
                                    <p><span>Last Login: </span><span>{{$user_log->ip_address}}</span></p>
                                    {{-- 2400:abc:1b5c:1900:19b5 --}}
                                </div>
                                <div class="about-profile-right col-sm">
                                    <div class="row profile-pic-row">
                                        <div class="col-sm">
                                            <img class="user-img-responsive" src="{{asset('assets/img/user.png')}}">
                                        </div>
                                        <div class="col-sm">

                                        </div>
                                    </div>
                                    <div class="profile-details-table">
                                        <table class="table">

                                            <tbody>

                                                <tr>
                                                    <td>Tiered fee schedule</td>
                                                    <td>Regular user</td>
                                                </tr>
                                                <tr>
                                                    <td>Trading volume</td>
                                                    <td>0.00000000 USDT</td>

                                                </tr>
                                                <tr>

                                                    <td>Lock position</td>
                                                    <td>0.00000000 DFT</td>

                                                </tr>
                                                <tr>
                                                    <td>Maker fees</td>
                                                    <td>0.15%</td>
                                                </tr>
                                                <tr>
                                                    <td>Taker fees</td>
                                                    <td>0.15%</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="profile-inner-row-2 row">
                                {{-- <p>Last login: 2021-01-03 16:45:43 IP address: 2400 : adc1 : 12f : 2200 : e4ac : 5109 : 9a1d
                                    : 6be9</p> --}}
                                    <p>Last login: {{date("d M, Y - H:i A", strtotime($user_log->created_at)) }}  IP address: {{$user_log->ip_address}}</p>
                            </div>
                            <div class="profile-inner-row-3">
                                <div class="row">
                                    <div class="col-sm-4 cl-1">Identity Verification</div>
                                    <div class="col-sm-4 cl-2">Submit Verification</div>
                                    <div class="col-sm-4 cl-3"><img
                                            src="{{asset('assets/img/warning-icon.png')}}" /><span>Unverified</span></div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-4 cl-1">2 Factor Verification</div>
                                    <div class="col-sm-4 cl-2">To Protect your account Security</div>
                                    <div class="col-sm-4 cl-3"><img
                                            src="{{asset('assets/img/warning-icon.png')}}" /><span>Off</span></div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            @include('components.footer')
        </div>
    </div>
@endsection
@push('scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#sidebarCollapse').on('click', function () {
                $('#sidebar').toggleClass('active');
                $(this).toggleClass('active');
            });
        });
        $(document).ready(function () {
            $('body').addClass("profile-pg");
        })

    </script>
@endpush
