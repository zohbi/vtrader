<!-- Navigation Start -->
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container-fluid">

        <button type="button" id="sidebarCollapse" class="navbar-btn">
            <span></span>
            <span></span>
            <span></span>
        </button>
        <button class="btn btn-dark d-inline-block d-lg-none ml-auto" type="button" data-toggle="collapse"
            data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
            aria-label="Toggle navigation">
            <i class="fas fa-align-justify"></i>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="nav navbar-nav ml-auto">
                <li class="nav-item active">
                    <a href="https://vtrader.planets01.com/" target="_blank" class="nav-item nav-link active nav-a-margin">HOME</a>
                </li>
                <li class="nav-item">
                    <a href="{{url('/customizeBoard')}}" class="nav-item nav-link nav-a-margin">TRADE</a>
                </li>
                <li class="nav-item">
                    <a href="https://vtrader.planets01.com/pricing" target="_blank" class="nav-item nav-link nav-a-margin" tabindex="-1">PRICING</a>
                </li>
                <li class="nav-item">
                    <a href="https://vtrader.planets01.com/faq" target="_blank" class="nav-item nav-link nav-a-margin" tabindex="-1">HELP</a>
                </li>
                <div class="navbar-nav ml-auto">
                    <li><a href="#" class="nav-item nav-link nav-a-margin" tabindex="-1">BALANCE</a></li>
                    @if (Illuminate\Support\Facades\Auth::check())
                        <div class="dropdown">
                            <button class="btn btn-secondary" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fa fa-user" aria-hidden="true"></i>
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                
                                <a href="/profile" class="dropdown-item" >
                                    Profile
                                </a>
                                <form action="/logout" method="POST">
                                    @csrf
                                    <button class="dropdown-item" type="submit">Logout</button>
                                </form>
                                <!-- <a class="dropdown-item" href="#">Another action</a>
                                <a class="dropdown-item" href="#">Something else here</a> -->
                            </div>
                        </div>
                    @else
                        <span class="input-group-append ">
                            <div class="input-group-text bg-transparent search-field">
                                <input class="bg-transparent" type="text" placeholder="Search" aria-label="Search">
                                <i class="fa fa-search"></i>
                            </div>
                        </span>
                        <a href="#" class="nav-item nav-link hide-sm login_scr">LOGIN</a>
                        <span class="dot"></span>
                        <a href="#" class="nav-item nav-link hide-sm reg_scr">REGISTER</a>
                    @endif
                </div>
            </ul>
        </div>
    </div>
</nav>
<!-- Navigation End -->
