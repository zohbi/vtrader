<nav id="sidebar">
    <div class="profile-sidebar-logo-area">
        <a href="#" class="navbar-brand">
            <img src="{{asset('assets/img/logo.png')}}" />
        </a>
    </div>
    <ul class="list-unstyled components">
        <h4 class="pro-side-head">Account</h4>
        <li class="">
            <a href="#">Basic Information</a>
        </li>
        <li>
            <a href="#">My balance</a>
        </li>
        <li>
            <a href="#">Financial Log</a>
        </li>
        <li>
            <a href="#">Notifications</a>
        </li>
        <h4 class="pro-side-head">My trades</h4>
        <li>
            <a href="#">Open Orders</a>
        </li>
        <li>
            <a href="#">Transaction History</a>
        </li>
        <li>
            <a href="#">Order History</a>
        </li>
        <li>
            <a href="#">Investment History</a>
        </li>
        <li>
            <a href="#">My Purchase</a>
        </li>
        <h4 class="pro-side-head">Security Center</h4>
        <li>
            <a href="#">Change Password</a>
        </li>
        <li>
            <a href="#">2 Factor Authentication</a>
        </li>
        <!-- <li>
            <a href="#">API Setting</a>
        </li> -->
    </ul>
</nav>