<div class="sticky-left-container">
	<ul class="sticky-left">
		<li class="daynite">
		<i class="fa fa-sun-o i1" aria-hidden="true"></i>
		<i class="fa fa-moon-o i2" aria-hidden="true"></i>
		<div class="btn-group-vertical dark-light-btn h-100 justify-content-center align-items-center">
			<div class="can-toggle demo-rebrand-1">
					<input id="d" type="checkbox">
				<label for="d">
				<div class="can-toggle__switch" data-checked="Light" data-unchecked="Dark"></div>
				</label>
			</div>
		</div>
		</li>
	</ul>
</div>