<!-- 9th Screen Start -->
<div class="ninth-cont">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 ninth-cont-one">
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12 text-center">
                <div class="row">
                    <div class="col-lg-3 col-md-3">
                        <h5 style="color: #54b9d2;">About</h5>
                        <ul class="ninth-ul">
                            <li><a class="foot-a" href="https://vtrader.planets01.com/about-us">About Us</a></li>
                            <li><a class="foot-a" href="#">Download APP</a></li>
                            <!-- <li><a class="foot-a" href="#">Notice Board</a></li>
                                <li><a class="foot-a" href="#">Media Assets</a></li> -->
                        </ul>
                    </div>
                    <div class="col-lg-3 col-md-3">
                        <h5 style="color: #54b9d2;">Terms of Service</h5>
                        <ul class="ninth-ul">
                            <li><a class="foot-a" href="https://vtrader.planets01.com/terms-of-service">Terms of Service</a></li>
                            <li><a class="foot-a" href="https://vtrader.planets01.com/privacy-policy">Privacy Policy</a></li>
                            <li><a class="foot-a" href="https://vtrader.planets01.com/anti-money-laundering">Anti Money Laundering</a></li>
                            <!-- <li><a class="foot-a" href="#">Fees</a></li> -->
                        </ul>
                    </div>
                    <div class="col-lg-3 col-md-3">
                        <h5 style="color: #54b9d2;">Service Support</h5>
                        <ul class="ninth-ul">
                            <li><a class="foot-a" href="https://vtrader.planets01.com/faq">Help/Faq</a></li>
                            <li><a class="foot-a" href="https://vtrader.planets01.com/contact-us">Contact Us</a></li>
                        </ul>
                    </div>
                    <div class="col-lg-3 col-md-3">
                        <h5 style="color: #54b9d2;">Get in Touch</h5>
                        <ul class="ninth-ul">
                            <li>
                                <a class="foot-a" href="#">
                                    <img src="{{asset('assets/img/facebook.png')}}" style="width: 25px;" />
                                </a> 
                                <a class="foot-a" href="#">
                                    <img src="{{asset('assets/img/linkedin.png')}}" style="width: 25px;" />
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12 text-color" style="text-align: center;padding: 0 0 60px 0;">
                <!-- <img src="{{asset('assets/img/logo-lg-white.png')}}" style="width: 150px;" class="footer-logo"> -->
                <p style="margin-bottom: 0px;padding-top: 50px;font-size: 14px;">Copyright © 2020-2021 VTRADER.COM ,
                    VTRADER LIMITED</p>
                <p style="margin-bottom: 0px;font-size: 14px;">All trading products offered through vtrader.com should
                    be considered HIGH RISK.</p>
                <p style="margin-bottom: 0px;font-size: 14px;">The Crypto Market is Volatile!</p>
            </div>
        </div>
    </div>
</div>
<!-- 9th Screen End -->
