import Vue              from 'vue';
import Vuex             from 'vuex';
import activeBoard      from './modules/activeBoard'
import userBoards       from './modules/userBoards'
import userTemplates    from './modules/userTemplates'

Vue.use(Vuex);

const debug = process.env.NODE_ENV !== 'production';

export default new Vuex.Store({
    /**
     * Assign the modules to the store
     */
    modules: { 
        activeBoard, userBoards, userTemplates
    },

    /**
     * If strict mode should be enabled
     */
    strict: debug
});
