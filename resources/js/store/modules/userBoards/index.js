export default {
    state: () => ({
        boards: []
    }),
    mutations: {
        setBoards(state, boards) {
            state.boards = boards
        },
        addBoards(state, boards) {
            state.boards.push(boards)
        },
        removeBoard(state, board) {
            state.boards = state.boards.filter(t => t.id != board.id)
        }
    },
    getters: {
    }
};
