export default {
    state: () => ({
        templates: []
    }),
    mutations: {
        setTemplates(state, templates) {
            state.templates = templates
        },
        addTemplate(state, template) {
            state.templates.push(template)
        },
        removeBoard(state, template) {
            state.template = state.template.filter(t => t.id != template.id)
        }
    },
    getters: {
    }
};
