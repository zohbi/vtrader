export default {
    state: () => ({
        id: "",
        title: "",
        widgets: [],
        editMode: "",
    }),
    mutations: {
        setActiveBoard(state, board) {
            state["id"]         = board.id
            state["title"]      = board.title
            state["widgets"]    = board.widgets
            state["editMode"]   = board.editMode
        },
        addWidget(state, widget) {
            state.widgets.push(widget)
        }
    },
    getters: {
    }
};
