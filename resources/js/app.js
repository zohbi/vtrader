require('./bootstrap');

import { App, plugin } 	from '@inertiajs/inertia-vue'
import Vue 				      from 'vue'
import VueDragResize 	  from 'vue-drag-resize'
Vue.use(plugin)
import store from "./store/index"
import swal from 'sweetalert2'
      window.swal = swal;

const toast = swal.mixin({
  toast: true,
  position: 'top-end',
  showConfirmButton: false,
  timer: 3000
});
window.toast = toast;
// import socketio 		from 'socket.io-client';
// import VueSocketIO 		from 'vue-socket.io';
// const SocketInstance = socketio.connect('https://uat.webtrade-api.com/transactional', {
//     query: {
//         token: ""
//     }
// });
// Vue.use(new VueSocketIO({
//     debug: true,
//     connection: SocketInstance
// }))
Vue.component('vue-drag-resize', VueDragResize)

const el = document.getElementById('app')

new Vue({
  store,
  render: h => h(App, {
    props: {
      initialPage: JSON.parse(el.dataset.page),
      resolveComponent: name => require(`./Pages/${name}`).default,
    },
  }),
}).$mount(el)
