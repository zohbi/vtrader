$(function () {
	var $tabButtonItem = $('#tab-button li'),
		$tabSelect = $('#tab-select'),
		$tabContents = $('.tab-contents'),
		activeClass = 'is-active';

	$tabButtonItem.first().addClass(activeClass);
	$tabContents.not(':first').hide();

	$tabButtonItem.find('a').on('click', function (e) {
		var target = $(this).attr('href');

		$tabButtonItem.removeClass(activeClass);
		$(this).parent().addClass(activeClass);
		$tabSelect.val(target);
		$tabContents.hide();
		$(target).show();
		e.preventDefault();
	});

	$tabSelect.on('change', function () {
		var target = $(this).val(),
			targetSelectNum = $(this).prop('selectedIndex');

		$tabButtonItem.removeClass(activeClass);
		$tabButtonItem.eq(targetSelectNum).addClass(activeClass);
		$tabContents.hide();
		$(target).show();
	});
});

jQuery(document).ready(function () {
	jQuery('button.lite-theme-cls').click(function () {
		jQuery('body').removeClass('lite-theme');
		jQuery('body').addClass('lite-theme');

	});
	jQuery('button.dark-theme-cls').click(function () {
		jQuery('body').removeClass('lite-theme');

	});
});
$(document).ready(function () {
	$(".tabcontent h3").click(function () {
		$(this).next('p').slideToggle("slow");
	});
});
$(document).ready(function () {
	$('nav.navbar.navbar-expand-md.navbar-dark .nav-bar a').removeClass('active');
});
$(document).ready(function () {
	$("[href]").each(function () {
		if (this.href == window.location.href) {
			$(this).removeClass("active2");
			$(this).addClass("active2");
		}
	});
});


//   $(function($) {
// 	var path = window.location.href;
// 	$('.side-nav-icon a').each(function() {
// 	 if (this.href === path) {
// 	  $(this).addClass('active5');
// 	 }
// 	});
//    });

// function myFunction(e) {
// 	var elems = document.querySelectorAll(".myactive");
// 	[].forEach.call(elems, function(el) {
// 	  el.classList.remove("myactive");
// 	});
// 	console.log($(this).attr('href'));
// 	e.target.className = "myactive";
// 	console.log("working");
//   }

function orderType(evt, allOrders) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(allOrders).style.display = "block";
  evt.currentTarget.className += " active";
 
}
$(document).ready(function(){
	$('.trade-1-col-right #defaultOpen').click();
	});

$(document).ready(function(){
	$('input#advanced_t:checkbox').change(function(){
	if($(this).is(":checked")) {
		$('.bottom_form').css("display","block");
	} else {
		$('.bottom_form').css("display","none");
	}
	});
});

$(function() {
	$('.data-hide').hide();
	$('#to-show-hide').change(function(){
		$('.data-hide').hide();
		$('#' + $(this).val()).show();
	});
});

// var abc = document.getElementById("ulwid").getElementsByTagName("li").length;
// var abch =94;
// var res = abch/abc;
// $("nav-item").style.width = "calc(100%/abc)";